import User from './User'
const Users = ({users,onToggle}) => {
    return (
        <div>
        {users.map((user)=>(
            <User key={user.id} user={user} 
            onToggle={onToggle}
            />

        ))}
            
        </div>
    )
}

export default Users