import PropTypes from 'prop-types'
import Button from './Button'
import User from './User'
const Header = ({title, onAnnul,onAdd,onDepass}) => {
    return (
        <div className='header'>

            <h1> {title}</h1>
            <Button color='blue' text='Depasser'onClick={onAdd}/>

            <Button  color='green' text='Anuuler' onClick={onAnnul}/>

            <Button color='red' text='Arréter' onClick={onDepass}/>
              
        </div>
    )
}  
Header.defaultProps ={
    title:'Page Admin',
}

Header.propTypes ={
     title:PropTypes.string.isRequired,
}

export default Header
