import {useState,useEffect} from 'react'
import AddPause from './component/AddPause'
import  Header from './component/Header'
//import User from './component/User'
import  Users from './component/Users'
//import  Users from './component/AddPause'


const App = () => {
  
  const [count, setCount] = useState()
  const [users,setUsers] = useState([
    {
      id:1,
      Pseudo:'IMAD',
      Password:'123',
      Type:'dev',
      team:[{
        Pseudo:'NABIL',
        Password:'456',
        Type:'dev1',
      }]
    },
    {
      id:2,
      Pseudo:'SAAD',
      Password:'789',
      Type:'dev',
      team:[{
        Pseudo:'YASSINE',
        Password:'147',
        Type:'dev2',
      }]
    },
    {
      id:3,
      Pseudo:'YAHYA',
      Password:'258',
      Type:'dev',
      team:[{
        Pseudo:'HAMZA',
        Password:'369',
        Type:'dev3',
      }]
    },
    ])

    useEffect(()=>{
      console.log(count)
      setCount(count+1)},[users])
      

    const [pauses,setPauses] = useState([
      {
        Id:1,
        DateDemande:'15/02/2022',
        DateDébut:'15/02/2022',
        Etat:'true',
        IdUser:3

        
      },
      {
        Id:2,
        DateDemande:'17/02/2022',
        DateDébut:'17/02/2022',
        Etat:'false',
        IdUser:1
      },
      {
        Id:3,
        DateDemande:'20/02/2022',
        DateDébut:'20/02/2022',
        Etat:'true',
        IdUser:2
      },
      ])

      var u;
      const onToggle =(id)=>{
      console.log({'id': id})
      u = id;
      }
       //ajout user
       var a = 1
        const addUser =()=>{
        const id =users.length + a
        const Pseudo = "anas"
        const Password ="123"
        const Type = "dev21"
        const team =[ ]
        const newUser = {id,Pseudo,Password,Type,team}
        setUsers([...users,newUser])
        

  
        }
        //changer l'emplacement
        const arreter = () => {
         
        const Filtrer = users.find(g => u == g.id)

        const nouvlist = users.filter((a) => a.id !== u);

          //  return le max  id users dans la list

          Filtrer.id =Math.max.apply(Math,users.map(function(A){return A.id}))+1
          
          // Nouveau Liste
          setUsers([...nouvlist,Filtrer])
          
          setCount(count +1)
         
        }
        
      //annuler user
      const annulUser =()=>{
      setUsers(users.filter((user)=>user.id!== u))}
  return (
    <div className='container'>
      <Header onAnnul= {annulUser} onAdd= {addUser} onDepass={arreter}  />
      <Users users={users} onToggle={onToggle}/>
      
    </div>
    
  )
}


export default App;
